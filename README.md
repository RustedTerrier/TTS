# TTS

### Tabs To Spaces

# Usage:
### Simply write `tts path_to_file` and tabs will be replaced with 4 spaces. With a different number do, `tts -s 2 path_to_file` or `tts --spaces 2 path_to_file`. For more options, write `tts -h`.

# Building: 
### Clone this repo, cd into the new directory, run `cargo run --release` and then `cp target/release/tts /usr/bin`. Then you can annhilate the tabs and bring order back to the files.

# Info:
### Language: Rust
### SLOC: 53
### Dependencies: Rust