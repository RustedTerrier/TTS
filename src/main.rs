use std::{env, fs, io::prelude::*};

fn main() {
    let info = parse_args();
    if info.path != "" {
        write_file(info.path, info.spaces);
    } else {
        println!("TTS\nUsage: TTS [options] [path_to_file]\n[options]\n-s or --spaces: number of spaces to replace each tab.\n-h or --help:   prints this message.\nRemember, spaces are better than tabs.")
    }
}

fn write_file(path: String, spaces: u8) {
    let file = fs::read_to_string(&path).expect("Could not read file");
    let mut file2 = fs::File::create(&path).expect("Could not write to file");
    file2.write_all(file.replace('\t', &" ".repeat(spaces as usize)[..])
                        .as_bytes())
         .expect("Could not write to file");
}

fn parse_args() -> Info {
    let args: Vec<String> = env::args().collect();
    let mut spaces = 4; // Amount of spaces instead of tabs
    let mut next = false; // Used for flags
    let mut path = "".to_string(); // Path to file
    for (i, c) in args.into_iter().enumerate() {
        if i != 0 {
            if next {
                spaces = c.parse::<u8>().unwrap();
                next = false;
            } else {
                if c.starts_with('-') {
                    match &c[..] {
                        | "-s" => next = true,
                        | "--spaces" => next = true,
                        | "-h" => path = "".to_owned(),
                        | "--help" => path = "".to_owned(),
                        | _ => {}
                    }
                } else {
                    path = c;
                }
            }
        }
    }

    Info { path,
           spaces }
}

struct Info {
    path:   String,
    spaces: u8
}
